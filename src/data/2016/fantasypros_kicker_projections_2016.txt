Stephen Gostkowski	NE	27.8	31.4	43.6	127
Mason Crosby	GB	25.7	30.5	44.4	121.4
Justin Tucker	BAL	28.5	33	35.1	120.6
Steven Hauschka	SEA	26.5	30.2	40.6	120.2
Graham Gano	CAR	25.8	31.2	42	119.6
Chris Boswell	PIT	26.2	29.9	39.3	118
Dan Bailey	DAL	25.9	29.4	39.7	117.5
Josh Brown	NYG	25.8	29.3	39.5	116.9
Chandler Catanzaro	ARI	24.8	28.8	42.1	116.5
Blair Walsh	MIN	26.7	31.7	34	114.2
Brandon McManus	DEN	25.6	30.1	35.9	112.7
Adam Vinatieri	IND	24.3	28.1	38.3	111.3
Mike Nugent	CIN	23.6	28.3	39.9	110.6
Cairo Santos	KC	23.9	29.5	38.5	110.1
Robbie Gould	CHI	25.1	29.9	34.3	109.7
Matt Prater	DET	24.3	28.3	34.1	107
Dan Carpenter	BUF	24	28.6	34.3	106.4
Nick Novak	HOU	23.7	28.1	35.2	106.2
Matt Bryant	ATL	22.8	28	35.6	104.1
Sebastian Janikowski	OAK	22.4	27.3	36.3	103.6
Nick Folk	NYJ	23.3	28	33.7	103.6
Dustin Hopkins	WAS	22.8	26.4	34.9	103.3
Jason Myers	JAC	22.9	27.8	33.1	101.8
Josh Lambo	SD	21.8	27	34.5	99.9
Roberto Aguayo	TB	21.6	26.3	34.7	99.3
Phil Dawson	SF	23	27.2	28.7	97.6
Greg Zuerlein	LA	21.9	28.8	31.1	96.8
Andrew Franks	MIA	20.8	26.3	32.7	95.1
Travis Coons	CLE	22.3	26.4	27.3	94.2
Cody Parkey	PHI	19.5	23.6	34.3	92.8
Caleb Sturgis	PHI	20.2	25.8	31.9	92.4
Ryan Succop	TEN	19.3	23.6	30.4	88.2
Kai Forbath	NO	17.4	22.1	33.2	85.4
Shaun Suisham	FA	17	20	21	72
Connor Barth	NO	5.3	6.2	21.8	37.5